const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_I = 10;

const SigninSchema = mongoose.Schema({
    Name:{
        type:String,
        required:true,
    },
    Username:{
        type:String,
        required:true,
        unique:true
    },
    Password:{
        type:String,
        required:true,
        minlength:4
    },
    Mobile:{
        type:Number,
        required:true,
        unique:true,
        maxlength:15
    }
})
SigninSchema.pre('save',function(next){
    const user=this;
  
        bcrypt.genSalt(SALT_I,function(err,salt){
            if(err) return next(err);
            bcrypt.hash(user.Password,salt,function(err,hash){
                 if(err) return next(err);
                  user.Password=hash;
                  next();
            })
        })
   
  
})

const Signin = mongoose.model('user-learners',SigninSchema);
const Signint = mongoose.model('user-teachers',SigninSchema);

module.exports = { Signin,Signint }