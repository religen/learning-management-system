const Express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const app = Express();
const bcrypt = require('bcrypt');
const SALT_I = 10;
const config = require('./config').get(process.env.NODE_ENV);
const { Subjects,TeachersLog,LearnersLog,ChangedCourse,NewCourse,Course } = require('./subjects');
const { Signin,Signint } = require('./Signin');

mongoose.Promise = global.Promise;
mongoose.connect(config.DATABASE);


app.use(cors());
app.use(bodyParser.json());
app.use(cookieParser());

app.use(Express.static('intern/build'))

  app.all('/', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*")
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next()
  });
  
app.get('/academy/subjects',(req,res)=>{
    
    Subjects.find(null,{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})
app.get('/academy-log/user-learners',(req,res)=>{
    
    Signin.find(null,{_id:0,Name:0,Password:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.get('/academy-log/user-teachers',(req,res)=>{
    
    Signint.find(null,{_id:0,Name:0,Password:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})
app.get('/academy/course',(req,res)=>{
    let name = req.query.course;
    
    Course.find({course : name},{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.get('/academy/change-course',(req,res)=>{
    ChangedCourse.find(null,{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.post('/academy/course/remove',(req,res)=>{
    let coursename = req.query.course;
    
    Course.remove({course : coursename},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(true);
    })
})

app.get('/academy/new-course',(req,res)=>{
    NewCourse.find(null,{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.post('/academy/change-course/remove',(req,res)=>{
    let coursename = req.query.course;
    
    ChangedCourse.remove({course : coursename},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(true);
    })
})
app.post('/academy/new-course/remove',(req,res)=>{
    let coursename = req.query.course;
    
    NewCourse.remove({course : coursename},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(true);
    })
})
app.get('/academy/subject',(req,res)=>{
    let named = req.query.subject;
    
    Subjects.find({subject : named},{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.get('/academy-log/learner-detail',(req,res)=>{
    let named = req.query.username;
    
    LearnersLog.find({Username : named},{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.get('/academy-log/learner-detail/all',(req,res)=>{
    
    LearnersLog.find(null,{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})
app.get('/academy-log/teacher-detail/all',(req,res)=>{
    
    TeachersLog.find(null,{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.get('/academy-log/teacher-detail',(req,res)=>{
    let named = req.query.username;
    
    TeachersLog.find({Username : named},{_id:0,__v:0}).exec((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.post('/academy/subjects',(req,res)=>{
    const Subject = new Subjects(req.body)

   Subject.save((err,doc)=>{
       if(err) return res.status(400).send(err);
       res.status(200).json({
           post:true
       })
   })
})

app.post('/academy/subjects/change',(req,res)=>{
    let sub = req.query.subject;
    Subjects.findOneAndUpdate({subject : sub},{$set : {courses :req.body}},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})

app.post('/academy/change-course',(req,res)=>{
    const course = new ChangedCourse(req.body)
   course.save((err,doc)=>{
       if(err) return res.status(400).send(err);
       res.status(200).json({
           post:true
       })
   })
})

app.post('/academy/new-course',(req,res)=>{
    const course = new NewCourse(req.body)
   course.save((err,doc)=>{
       if(err) return res.status(400).send(err);
       res.status(200).json({
           post:true
       })
   })
})
app.post('/academy/course',(req,res)=>{
    const course = new Course(req.body)
   course.save((err,doc)=>{
       if(err) return res.status(400).send(err);
       res.status(200).json({
           post:true
       })
   })
})
app.post('/academy/signin-learner',(req,res)=>{
    const sign = new Signin({
      Name:req.body.Name,
      Username:req.body.Username,
      Password:req.body.Password,
      Mobile:req.body.Mobile
    })   
    sign.save((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(true)
    })
})
app.post('/academy/signin-learner-det',(req,res)=>{  
    const learn = new LearnersLog({
        Username:req.body.Username,
        Name:req.body.Name,
        Age : '',
        Bio : '',
        Gender : '',
        Email : '',
        Mobile:req.body.Mobile,
        Password:req.body.Password,
        Courses : []
      })
    learn.save((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(true)
    })
})

app.post('/academy/signin-teacher-det',(req,res)=>{  
    const teach = new TeachersLog({
        Username:req.body.Username,
        Name:req.body.Name,
        Age : '',
        Bio : '',
        Gender : '',
        Email : '',
        Mobile:req.body.Mobile,
        Password:req.body.Password,
        Courses : []
      })
    teach.save((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(true)
    })
})
app.post('/academy/signin-teacher',(req,res)=>{
    const sign = new Signint({
      Name:req.body.Name,
      Username:req.body.Username,
      Password:req.body.Password,
      Mobile:req.body.Mobile
    })
    sign.save((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(true)
    })
})
app.post('/academy/login-learner',(req,res)=>{
    Signin.findOne({'Username':req.body.Username},(err,user)=>{
        if(err) return res.status(400).send(err);
        if(!user) return res.json({isAuth:false,message:'Username not found'});
        bcrypt.compare(req.body.Password,user.Password,(err,isMatch)=>{
            if(err) return res.status(400).send(err);
            res.status(200).send(isMatch);
        })
   })
})
app.post('/academy/login-teacher',(req,res)=>{
    Signint.findOne({'Username':req.body.Username},(err,user)=>{
        if(err) return res.status(400).send(err);
        if(!user) return res.json({isAuth:false,message:'Username not found'});
        bcrypt.compare(req.body.Password,user.Password,(err,isMatch)=>{
            if(err) return res.status(400).send(err);
            res.status(200).send(isMatch);
        })
   })
})

app.post('/academy-log/login-learner/update',(req,res) =>{
    bcrypt.genSalt(SALT_I,function(err,salt){
        if(err) return Console.log(err);
        bcrypt.hash(req.body.Password,salt,function(err,hash){
             if(err) return Console.log(err);
             Signin.findOneAndUpdate(
                {'Username':req.body.Username},
                {$set: {Password:hash}},
                (err,doc)=>{
                    if(err) return console.log(err);
                    res.status(200).send(doc)
                }           
            )
        })
    })
})

app.post('/academy-log/login-teacher/update',(req,res) =>{
    bcrypt.genSalt(SALT_I,function(err,salt){
        if(err) return Console.log(err);
        bcrypt.hash(req.body.Password,salt,function(err,hash){
             if(err) return Console.log(err);
             Signint.findOneAndUpdate(
                {'Username':req.body.Username},
                {$set: {Password:hash}},
                (err,doc)=>{
                    if(err) return console.log(err);
                    res.status(200).send(doc)
                }           
            )
        })
    })
})
app.post('/academy-log/learner-profile/remove',(req,res)=>{
    let username = req.query.username;
    
    LearnersLog.remove({Username : username},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(true);
    })
})

app.post('/academy-log/teacher-profile/remove',(req,res)=>{
    let username = req.query.username;
    
    TeachersLog.remove({Username : username},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(true);
    })
})
app.post('/academy/teacher-profile/remove',(req,res)=>{
    let username = req.query.username;
    
    Signint.remove({Username : username},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(true);
    })
})
app.post('/academy/learner-profile/remove',(req,res)=>{
    let username = req.query.username;
    
    Signin.remove({Username : username},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(true);
    })
})
app.post('/academy-log/teacher-profile/update',(req,res)=>{
    let sub = req.body.Username
    let courses = req.body.Courses
      TeachersLog.findOneAndUpdate({Username : sub},{$set : {Courses :courses}},(err,doc)=>{
        if(err) return res.status(400).send(err);
        res.send(doc);
    })
})
app.post('/academy-log/teacher-profile/change',(req,res)=>{
    const learn = new TeachersLog({
        Username:req.body.Username,
        Name:req.body.Name,
        Age : req.body.Age,
        Bio : req.body.Bio,
        Gender : req.body.Gender,
        Email : req.body.Email,
        Mobile:req.body.Mobile,
        Password:req.body.Password,
        Courses :req.body.Courses
      })
      learn.save((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(true)
    })
})
app.post('/academy-log/learner-profile/change',(req,res)=>{
    const learn = new LearnersLog({
        Username:req.body.Username,
        Name:req.body.Name,
        Age : req.body.Age,
        Bio : req.body.Bio,
        Gender : req.body.Gender,
        Email : req.body.Email,
        Mobile:req.body.Mobile,
        Password:req.body.Password,
        Courses :req.body.Courses
      })
      learn.save((err,doc)=>{
        if(err) return res.status(400).send(err);
        res.status(200).send(true)
    })
})
if(process.env.NODE_ENV === 'production'){
    const path = require('path');
    app.get('/*',(req,res)=>{
        res.semdfile(path.resolve(__dirname,'../intern','build','index.html'))
    })
}
const port = process.env.PORT || 3001

app.listen(port,()=>{
    console.log(`succesfull on port ${port}`);
})