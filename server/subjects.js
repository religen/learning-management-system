const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_I = 10;

const subjectSchema = mongoose.Schema({
    subject : String,
    image : String,
    courses : Array
})
const courseSchema = mongoose.Schema({
    course : String,
    subject : String,
    description : String,
    titles : [
        {
            title: String,
            videos: Array
        }
    ]
})
const newCourseSchema = mongoose.Schema({
    Username:String,
    course : String,
    subject : String,
    description : String,
    titles : [
        {
            title: String,
            videos: Array
        }
    ]
})
const learnersSchema = mongoose.Schema({
    Username : String,
    Name : String,
    Age : String,
    Bio : String,
    Gender : String,
    Email : String,
    Mobile : Number,
    Password : String,
    Courses : Array
})
learnersSchema.pre('save',function(next){
    const user=this;
  
        bcrypt.genSalt(SALT_I,function(err,salt){
            if(err) return next(err);
            bcrypt.hash(user.Password,salt,function(err,hash){
                 if(err) return next(err);
                  user.Password=hash;
                  next();
            })
        })
   
    })

const Subjects = mongoose.model('subjects',subjectSchema)
const NewCourse = mongoose.model('courses-requested',newCourseSchema)
const ChangedCourse = mongoose.model('courses-changed',newCourseSchema)
const Course = mongoose.model('courses',courseSchema)
const LearnersLog = mongoose.model('learners-log',learnersSchema)
const TeachersLog = mongoose.model('teachers-log',learnersSchema)

module.exports = { Subjects,ChangedCourse,NewCourse,Course,LearnersLog,TeachersLog }